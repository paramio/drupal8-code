<?php

/* {# inline_template_start #}<br><small>{{ context_title }}: <span lang="en">{{ context }}</span></small> */
class __TwigTemplate_c7e2cd8453afd5342cf45055d470d30a0fa31e59a3634a2aa3b5f1d0d7411f44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<br><small>";
        echo twig_drupal_escape_filter($this->env, (isset($context["context_title"]) ? $context["context_title"] : null), "html", null, true);
        echo ": <span lang=\"en\">";
        echo twig_drupal_escape_filter($this->env, (isset($context["context"]) ? $context["context"] : null), "html", null, true);
        echo "</span></small>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<br><small>{{ context_title }}: <span lang=\"en\">{{ context }}</span></small>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
