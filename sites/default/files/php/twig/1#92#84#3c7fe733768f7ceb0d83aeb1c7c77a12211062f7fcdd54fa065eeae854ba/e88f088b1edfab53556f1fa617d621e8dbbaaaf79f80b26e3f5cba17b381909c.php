<?php

/* core/themes/classy/templates/field/image-style.html.twig */
class __TwigTemplate_92843c7fe733768f7ceb0d83aeb1c7c77a12211062f7fcdd54fa065eeae854ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 18
        echo twig_drupal_escape_filter($this->env, (isset($context["image"]) ? $context["image"] : null), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "core/themes/classy/templates/field/image-style.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 18,);
    }
}
